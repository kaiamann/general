Elektronische Labor-Notizbücher (ELNs)
======================================

KMW, 31. Mai 2021

## Allgemein

Wikipedia: https://de.wikipedia.org/wiki/Elektronisches_Laborbuch 

### Grundfunktionalität:
Laborjournal https://de.wikipedia.org/wiki/Laborjournal ;
"ein Notizbuch, in dem die Planung, Durchführung und Auswertung
von wissenschaftlichen Experimenten dokumentiert wird."

- chronologische Dokumentation der wissenschaftlichen Tätigkeit
- für eine Person mit Fachwissen nachvollziehbar und ggf. wiederholbar
- Messwerte und Rechnungen direkt notieren
- Rohdaten in Form von Schreiberausdrucken, Fotografien, Röntgenfilmen etc.
  direkt übernehmen ("einkleben")
- Schreib- und Rechenfehler durchstreichen (nicht schwärzen oder löschen);
  Änderungsgeschichte führen
- Inhaltsverzeichnis angelegt

Drei Arten:
1. Allgemeine Systeme,
   wie zum Beispiel Software zur Textverarbeitung
   oder Notiz-Software
2. Dedizierte Systeme
   zur Verwaltung elektronischer Laborbücher
3. Spezialisierte Systeme
   zur Verwaltung elektronischer Laborbücher
   in einer bestimmten Wissenschaftsdisziplin

Nennt die folgenden Systeme:
CERF (Lab-Ally LLC),	
eLabFTW (Nicolas Carpi),
lab folder (Labfolder GmbH),
LabTwin (LabTwin GmbH),
openBIS	(ETH Zürich),
SciNote (SciNote LLC).

## OpenBIS

https://openbis.ch/ 

https://de.wikipedia.org/wiki/OpenBIS 

## LabFolder

kommerziell

## eLabFTW

https://www.elabftw.net/ 

## FURTHR



## Chemotion

https://www.chemotion.net/chemotionsaurus/index.html 

## Sciformation

http://sciformation.com/ 

## 
